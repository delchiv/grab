# coding: utf-8

from django.db import models

# Create your models here.

class Link(models.Model):
    id = models.CharField(max_length=64, primary_key=True)
    url = models.CharField(max_length=1000)
    title = models.CharField(max_length=1000, blank=True, null=True)
    h1 = models.CharField(max_length=1000, blank=True, null=True)
    img = models.ImageField(upload_to='uploads/%Y/%m/%d/', blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s (%s)' % (self.url, self.id)

