# coding: utf-8

from django import forms
from django.forms import formsets
from django.utils.translation import ugettext_lazy as _


class LinkForm(forms.Form):
    url = forms.CharField(max_length=1000, label=_('Url to grab'), widget=forms.TextInput(attrs={'class':'form-control'}))

LinkFormset = formsets.formset_factory(LinkForm, extra=1, max_num=5)
