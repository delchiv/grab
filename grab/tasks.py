# Create your tasks here           \
from __future__ import absolute_import, unicode_literals

import os
import time
import urllib
import urllib2
from random import random

from celery import shared_task
from bs4 import BeautifulSoup

from django.conf import settings
from django.core.files import File
from django.template.loader import render_to_string
from django.utils.translation import ugettext as _

from .models import Link


@shared_task(bind=True)
def parse_url(self, url):
    title = ''
    h1 = ''
    image_url =''
    image = [0]

    self.update_state(state=_('Retrieving html...'), meta={'percent': 10})
    try:
        page = urllib2.urlopen(url)
    except Exception as e:
        title = str(e)
    else:
        self.update_state(state=_('Converting html...'), meta={'percent': 20})
        soup = BeautifulSoup(page)
    
        self.update_state(state=_('Retrieving html...'), meta={'percent': 30})
        title = soup.title
        if title: title = title.string
    
        self.update_state(state=_('Retrieving H1...'), meta={'percent': 40})
        h1 = soup.h1
        if h1: h1 = h1.string
    
        self.update_state(state=_('Retrieving image url...'), meta={'percent': 50})
        image_url = soup.img['src']
    
        self.update_state(state=_('Downloading image...'), meta={'percent': 60})
        try: image = urllib.urlretrieve(url+image_url.replace('//','/'), os.path.join(settings.MEDIA_ROOT, os.path.basename(image_url)))
        except: pass
    finally:
        time.sleep(0.5)
        self.update_state(state=_('Retrieving db model...'), meta={'percent': 70})
        link = Link.objects.get(pk=self.request.id)

        self.update_state(state=_('Updating db model...'), meta={'percent': 80})
        link.title = title
        link.h1 = h1

        self.update_state(state=_('Updating db model...'), meta={'percent': 90})
        if image_url and image and image[0]:
            link.img.save(os.path.abspath(os.path.join(settings.MEDIA_ROOT, os.path.basename(image_url))), File(open(image[0])))
        link.save()

