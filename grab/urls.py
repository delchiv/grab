from django.conf.urls import patterns, include, url

from .views import IndexView, InfoView, CreateView, DeleteView

urlpatterns = patterns('',
    url(r'^$', IndexView.as_view(), name='home'),
    url(r'^info/$', InfoView.as_view(), name='info'),
    url(r'^create/$', CreateView.as_view(), name='create'),
    url(r'^delete/$', DeleteView.as_view(), name='delete'),
)
