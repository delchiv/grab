# coding: utf-8

import json
import datetime
from celery import states

from django.core.paginator import Paginator
from django.conf import settings
from django.http.response import JsonResponse
from django.shortcuts import render
from django.template.loader import render_to_string
from django.views.generic import View, TemplateView
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator

from grab_project import celery_app
from .tasks import parse_url
from .forms import LinkFormset
from .models import Link

# Create your views here.

class IndexView(TemplateView):
    
    template_name = 'grab/index.html'

    def get_context_data(self, *kwargs):
        links = self.request.session.get('links', [])
        context = super(IndexView, self).get_context_data(*kwargs)
        paginator = Paginator(Link.objects.filter(id__in=links).order_by('-created'), settings.GRAB_PER_PAGE)

        context['formset'] = LinkFormset
        context['paginator'] = paginator

        return context

    def get(self, request):
        if request.is_ajax():
            page = request.GET.get('page', 1)
            paginator = self.get_context_data().get('paginator')
            return JsonResponse({'data': render_to_string('grab/_links.html', {'links': paginator.page(page)})})
        return super(IndexView, self).get(request)


class CreateView(View):

    def post(self, request):
        links = request.session.get('links', [])

        formset = LinkFormset(request.POST)
        countdown = request.POST.get('countdown', 0)
        if countdown:
            countdown = (datetime.datetime.strptime(countdown, '%m/%d/%Y %I:%M %p') - datetime.datetime.now()).total_seconds()
        if formset.is_valid():
            for form in formset[::-1]:
                if form.is_valid():
                    url = form.cleaned_data.get('url')
                    if url:
                        task = parse_url.apply_async((url,), countdown=countdown)
                        link = Link.objects.create(id=task.id, url=url)
                        links.append(link.id)
                else:                      
                    pass
        
        paginator = Paginator(links, settings.GRAB_PER_PAGE)
        request.session['links'] = links
        return JsonResponse({'success': True, 'num_pages': paginator.num_pages })


class DeleteView(View):

    def post(self, request, *args, **kwargw):
        task_id = request.POST.get('task_id')
        task = celery_app.AsyncResult(task_id)
        task.revoke(terminate=True)
        return JsonResponse({'success': True})

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(DeleteView, self).dispatch(*args, **kwargs)


class InfoView(View):

    def post(self, request, *args, **kwargs):
        result = []
        for task_id in request.POST.values():
            task = celery_app.AsyncResult(task_id)
            state = task.state
            cls = 'progress-bar-info'
            info = ''
            if state == states.SUCCESS:
                percent = 100
                link = Link.objects.get(pk=task_id)
                info = render_to_string('grab/_link_info.html', {'link': link})
            elif state == states.FAILURE:
                percent = 100
                cls = 'progress-bar-danger'
            elif state == states.REVOKED:
                percent = 100
                cls = 'progress-bar-warning'
            else:
                try: percent = task.info.get('percent', 0)
                except: percent = 0
            message = state
            result.append({'id': task_id, 'percent': percent, 'message': message, 'cls': cls, 'info':info})
        return JsonResponse({'data': result})
    
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(InfoView, self).dispatch(*args, **kwargs)

